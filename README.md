#Evolution Gaming Home Task

Task is created with pure React and Websockets.
Configuration (webpack and express) is created only for development mode

To start development run ---> "npm install" and then "npm start"

Go to http://localhost:3000/ or use your IP which is written in your terminal.

Project is using CSS modules , which means that classNames are local , not global , and css className output name is generated using file name and className , for example, we have file "login-view" and <div className={styles.modal} /> , className output will be "login-view\_\_modal".

If project will grow, it's very useful to use redux together with react (for state management) and then use container && template structure where containers would provide a bridge between our Redux Actions/Reducers and our Atomic Design Templates. This would provide a separation of concerns. And it would be easier to test.
