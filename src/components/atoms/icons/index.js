import React, { Component } from "react"
import styles from "./icons.scss"

export const Participant = ({ className }) => (
	<svg className={className} viewBox="0 0 1000 1000">
		<g>
			<path
				fill="currentColor"
				d="M243.1,278.1c0,111,70.3,205.6,168.9,241.3C190.6,559.6,21,748.6,10,978.9h980c-10.8-230.3-180.4-419.3-401.8-459.5
			c98.6-35.7,168.9-130.4,168.9-241.3c0-142-114.9-257-257-257C358.1,21.1,243.1,136,243.1,278.1z M289.9,278.1
			C289.9,162,384,67.8,500.1,67.8s210.2,94.1,210.2,210.3c0,116.1-94.1,210.2-210.2,210.2S289.9,394.2,289.9,278.1z M938.6,932.2
			h-877c33.6-211.9,217.2-373.8,438.5-373.8S905,720.3,938.6,932.2z"
			/>
		</g>
	</svg>
)

const icons = {
	participant: [Participant],
}

export default class Icon extends Component {
	render() {
		const { type, className } = this.props
		const [Icon] = icons[type]

		return (
			<span className={className}>
				{Icon && <Icon className={styles.svgSize} />}
			</span>
		)
	}
}
