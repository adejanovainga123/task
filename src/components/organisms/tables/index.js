import React, { Component } from "react"
import styles from "./tables.scss"

import Icon from "../../atoms/icons"

import { ALL_POSSIBLE_PARTICIPANTS_IN_TABLE } from "../../../constants"

export default class Tables extends Component {
	displayActiveParticipants(table) {
		return Array.from({ length: table.participants }).map((e, i) => (
			<div className={styles.participantIcon}>
				<span key={i} className={styles.currentTableParticipants}>
					<Icon type="participant" />
				</span>
			</div>
		))
	}

	displayFreePlacesForParticipants(table) {
		const allPossibleParticipantsInTable = ALL_POSSIBLE_PARTICIPANTS_IN_TABLE

		return Array.from({
			length: allPossibleParticipantsInTable - table.participants,
		}).map((e, i) => (
			<div key={i} className={styles.participantIcon}>
				<Icon type="participant" />
			</div>
		))
	}

	render() {
		const { tables } = this.props

		return (
			<div className={styles.tables}>
				{(tables || []).map(table => (
					<div className={styles.item} key={table.id}>
						<p>{table.name}</p>
						<div className={styles.participants}>
							{this.displayActiveParticipants(table)}
							{this.displayFreePlacesForParticipants(table)}
						</div>
					</div>
				))}
			</div>
		)
	}
}
