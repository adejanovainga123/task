import React, { Component } from "react"
import styles from "./login-view.scss"

export default class LoginView extends Component {
	constructor(props) {
		super(props)

		this.state = {
			userName: "",
			password: "",
			isLoginFailed: false,
		}

		this.logIn = this.logIn.bind(this)
		this.handleInputChange = this.handleInputChange.bind(this)
	}

	handleInputChange(event) {
		const target = event.target
		const { value, name } = target

		this.setState({
			[name]: value,
		})
	}

	async logIn() {
		const { websocket } = this.props
		const { userName, password } = this.state

		const loginCredentials = JSON.stringify({
			$type: "login",
			username: userName,
			password: password,
		})
		const tables = JSON.stringify({
			$type: "subscribe_tables",
		})

		await websocket.send(loginCredentials)
		websocket.send(tables)
	}

	render() {
		const { isLoginFailed } = this.props

		return (
			<div className={styles.modal}>
				<div className={styles.modalContent}>
					<div>
						<h4>Please Login!</h4>
						<p>UserName: user1234</p>
						<p>password: password1234</p>
						<form>
							<input
								name="userName"
								type="text"
								placeholder="userName"
								onChange={this.handleInputChange}
							/>
							<input
								type="password"
								name="password"
								placeholder="password"
								onChange={this.handleInputChange}
							/>
							<button type="button" onClick={this.logIn}>
								Log In
							</button>
						</form>
						{isLoginFailed && <p>Wrong userName or password</p>}
					</div>
				</div>
			</div>
		)
	}
}
