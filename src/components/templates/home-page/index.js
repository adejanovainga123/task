import React, { Component } from "react"
import LoginView from "../../organisms/login-view"
import Tables from "../../organisms/tables"

export default class HomePage extends Component {
	constructor(props) {
		super(props)

		this.state = {
			isLoginFailed: false,
			isLoginSuccessful: false,
			isLoginViewOpened: true,
			tables: [],
			visibleTableCount: 30,
			currentTableCount: 40,
			isFetching: false,
			tableList: [],
		}

		this.loadingRef = React.createRef()

		this.socket = new WebSocket(
			"wss://js-assignment.evolutiongaming.com/ws_api",
		)
		this.loadDataOnScroll = this.loadDataOnScroll.bind(this)
	}

	componentDidMount() {
		const { socket } = this

		socket.onopen = () => this.onSocketOpen()
		socket.onmessage = message => this.onSocketData(message)
	}

	componentWillUnmount() {
		window.removeEventListener("scroll", this.loadDataOnScroll)
	}

	removeTable(id) {
		const { tableList } = this.state

		this.setState({
			tableList: tableList.filter(table => {
				if (table.id !== id) {
					return table
				}
			}),
		})
	}

	loadInitialContent() {
		const { visibleTableCount, tableList } = this.state

		this.setState({
			tables: tableList.slice(0, visibleTableCount),
		})
	}

	onSocketOpen() {
		console.log("Connection established!")
	}

	onSocketData(message) {
		const msg = JSON.parse(message.data)

		switch (msg.$type) {
			case "login_failed":
				this.setState({ isLoginFailed: true })
				break
			case "login_successful":
				this.setState({
					isLoginSuccessful: true,
					isLoginFailed: false,
					isLoginViewOpened: false,
				})
				break
			case "table_list":
				this.setState({ tableList: msg.tables })
				this.loadInitialContent()
				window.addEventListener("scroll", this.loadDataOnScroll)
				break
			case "table_removed":
				this.removeTable(msg.id)
				this.setState({
					tables: this.state.tableList.slice(0, this.state.currentTableCount),
				})
				console.log(msg.id)
				break
		}
	}

	loadDataOnScroll() {
		const {
			currentTableCount,
			tableList,
			isFetching,
			visibleTableCount,
		} = this.state

		const loadingElement = this.loadingRef.current
		const rect = loadingElement && loadingElement.getBoundingClientRect()
		const isAtEndOfHorizontalScroll =
			(rect || {}).right <= window.innerWidth + 1

		if (currentTableCount > tableList.length) {
			return
		}

		if (isAtEndOfHorizontalScroll) {
			if (!isFetching) {
				this.setState({ isFetching: true })

				const totalTableCount = currentTableCount + visibleTableCount

				this.setState({
					isFetching: false,
					currentTableCount: totalTableCount,
					tables: tableList.slice(0, totalTableCount),
				})
			}
		}
	}

	render() {
		const {
			isLoginFailed,
			tables,
			isLoginViewOpened,
			isLoginSuccessful,
			currentTableCount,
			tableList,
		} = this.state

		return (
			<React.Fragment>
				<Tables tables={tables} />
				{isLoginViewOpened && (
					<LoginView
						isLoginFailed={isLoginFailed}
						isLoginSuccessful={isLoginSuccessful}
						websocket={this.socket}
					/>
				)}
				{currentTableCount !== tableList.length &&
					currentTableCount < tableList.length && (
						<div ref={this.loadingRef}>Loading...</div>
					)}
			</React.Fragment>
		)
	}
}
