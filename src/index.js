import React from "react"
import { render } from "react-dom"
import HomePage from "../src/components/templates/home-page"

import "./styles/main.scss"

export const App = () => <HomePage />

render(<App />, document.getElementById("app"))

if (module.hot) {
	module.hot.accept()
}
