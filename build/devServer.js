const path = require("path")
const express = require("express")
const webpack = require("webpack")
const ip = require("ip")
const config = require("./webpack.config.js")
const webpackHotMiddleware = require("webpack-hot-middleware")
const webpackDevMiddleware = require("webpack-dev-middleware")

const app = express()

const compiler = webpack(config)

app.use(
	webpackDevMiddleware(compiler, {
		noInfo: true,
		publicPath: "/",
	}),
)

app.use(webpackHotMiddleware(compiler))

app.use(express.static("dist"))

app.get("*", (req, res) => {
	res.sendFile(path.join(__dirname, "../dist/index.html"))
})

const PORT = process.env.PORT || 3000

app.listen(PORT, "0.0.0.0", () => {
	console.log(
		`
		-- Ingas App --
		Listening: ${ip.address()}:${PORT}
		`.replace(/\t/g, ""),
	)
})
