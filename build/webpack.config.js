const path = require("path")
const webpack = require("webpack")
const autoprefixer = require("autoprefixer")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
	mode: "development",
	entry: {
		index: ["webpack-hot-middleware/client", "./src/index.js"],
	},
	output: {
		path: path.resolve("dist"),
		publicPath: "/",
		filename: "bundle.js",
	},
	devtool: "source-map",
	resolve: {
		extensions: [".js"],
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: "babel-loader",
			},
			{
				test: /\.scss$/,
				use: [
					"style-loader",
					{
						loader: `css-loader`,
						options: {
							importLoaders: 1,
							localIdentName: `[name]__[local]--[hash:base64:5]`,
							sourceMap: true,
						},
					},
					{
						loader: "postcss-loader",
						options: {
							sourceMap: true,
							plugins: [autoprefixer()],
						},
					},
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							includePaths: [
								path.resolve("src/styles"),
								path.resolve("src/components"),
								path.resolve("node_modules"),
							],
						},
					},
				],
				include: path.resolve("src"),
			},
		],
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: "./src/index.html",
			filename: "index.html",
			excludeChunks: ["server"],
			inject: true,
		}),
	],
}
